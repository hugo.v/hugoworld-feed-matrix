#!/usr/bin/env python
import requests
from datetime import datetime

articlePath = "/hugoworld-matrix/articles.json"

today = datetime.today().strftime('%Y-%m-%d')
x = requests.get(
    f"https://hugoworld-feed-service.herokuapp.com/api/articles?date={today}")

if (x.text != "[]"):
    f = open(articlePath, "w+")
    f.write(x.text)
    f.close()
