FROM python:3

WORKDIR /hugoworld-matrix
ADD . /hugoworld-matrix

# Packages install
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get install -y cron python3-pip
RUN pip install -r requirements.txt

# Cron setup
COPY src/dailyFetch/cron /etc/cron.d/dailyfetch
RUN chmod 0744 src//dailyFetch/dailyFetch.py
RUN chmod 0644 /etc/cron.d/dailyfetch

ENV PYTHONUNBUFFERED 1

CMD cron -f