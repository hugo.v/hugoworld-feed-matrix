DOCKER=docker
IMAGE=hugoworld-feed-matrix

install: build start

bash:
	@$(DOCKER) exec -it $(IMAGE) bash

logs:
	$(DOCKER) exec -it $(IMAGE) tail -f data/articles.json

build:
	@$(DOCKER) build -t $(IMAGE) .

delete:
	@echo "Deleting container"
	@$(DOCKER) stop $(IMAGE)
	@$(DOCKER) rm $(IMAGE)

start:
	@$(DOCKER) run -d --name $(IMAGE) $(IMAGE)
